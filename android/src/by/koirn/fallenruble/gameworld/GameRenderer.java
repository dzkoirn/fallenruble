package by.koirn.fallenruble.gameworld;

import android.content.Context;
import by.koirn.fallenruble.AssetsManager;
import by.koirn.fallenruble.gameworld.gameobjects.CoinObject;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by dmitry on 24.01.16.
 */
public class GameRenderer {

    private final OrthographicCamera cam;
    private final SpriteBatch mBatcher;
    private final int mGameHeight;
    private final int mGameWidth;
    private final int mMidPointY;
    private final int mRuleWidth;
    private final int mRuleX;
    private final int mScoreX;
    private final int mScoreY;
    private final Context mContext;

    public GameRenderer(Context context, int screenWidth, int screenHeight) {
        // The word "this" refers to this instance.
        // We are setting the instance variables' values to be that of the
        // parameters passed in from GameScreen.
        mGameHeight = screenHeight;
        mGameWidth = screenWidth;
        mMidPointY = screenHeight / 2;
        mRuleWidth = mGameWidth / 4;
        mRuleX = mGameWidth - mRuleWidth + 2;

        mScoreX = 0;
        mScoreY = 20;

        cam = new OrthographicCamera();
        cam.setToOrtho(true, mGameWidth, mGameHeight);

        mBatcher = new SpriteBatch();
        // Attach batcher to camera
        mBatcher.setProjectionMatrix(cam.combined);

        mContext = context;
    }

    public void renderWorld(final GameWorld world) {
        CoinObject coin = world.getCoin();

        // We draw a black background. This prevents flickering.
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Begin SpriteBatch
        mBatcher.begin();
        // Disable transparency
        // This is good for performance when drawing images that do not require
        // transparency.
        mBatcher.disableBlending();
        mBatcher.draw(AssetsManager.sBackgroundTextureRegion, 0, 0, mGameWidth, mGameHeight);

        mBatcher.draw(AssetsManager.sRuleTextureRegion, mRuleX, 0, mRuleWidth, mGameHeight);

        // The bird needs transparency, so we enable that again.
        mBatcher.enableBlending();

        // Draw coin at its coordinates. Retrieve the Animation object from
        // Pass in the runTime variable to get the current frame.
        mBatcher.draw(AssetsManager.sCoinTextureRegion,
                coin.getX(), coin.getY(),
                coin.getWidth() / 2.0f, coin.getHeight() / 2.0f,
                coin.getWidth(), coin.getHeight(),
                1, 1, coin.getRotation());

        // Convert integer into String
        String course = world.getCourseString(mContext);
        String score = world.getScoreString(mContext);

        // Draw text
        AssetsManager.sFont.draw(mBatcher, course, mScoreX, mScoreY);
        AssetsManager.sFont.draw(mBatcher, score, mScoreX, mScoreY + AssetsManager.sFont.getLineHeight() + 5);

        // End SpriteBatch
        mBatcher.end();
    }

}
