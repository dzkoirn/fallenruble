package by.koirn.fallenruble.gameworld;

import android.content.Context;
import by.koirn.fallenruble.AssetsManager;
import by.koirn.fallenruble.R;
import by.koirn.fallenruble.gameworld.gameobjects.CoinObject;

/**
 * Created by dmitry on 24.01.16.
 */
public class GameWorld {

    private static final int MAX_COURSE = 100;

    private final CoinObject mCoin;
    private final int mWorldWidth;
    private final int mWorldHeight;
    private final OnGameOverListener mListener;

    private long mPrevUpdateTime = 0;

    private int minCourse = 0;
    private float mCourse = 0;
    private int mScore = 0;

    public GameWorld(final OnGameOverListener listener, final int width, final int height) {
        mListener = listener;
        mWorldWidth = width;
        mWorldHeight = height;

        int coinSize = CoinObject.calculateCoinSizeFromScreenSize(mWorldWidth, mWorldHeight);
        this.mCoin = new CoinObject(width/2 - coinSize/2, mWorldHeight/2, coinSize, coinSize);

        calculateCourse();
        minCourse = Math.round(mCourse);
    }

    private void calculateCourse() {
        mCourse = mCoin.getY() * MAX_COURSE / mWorldHeight;
    }

    public void update(final float delta) {
        updateGameplay(delta);
    }

    private void updateGameplay(float delta) {
        mCoin.move(delta);
        mCoin.rotate(4 * delta);
        updateScore();
        if (mCoin.getY() > mWorldHeight) {
            AssetsManager.sDead.play();
            mListener.gameOver(mScore, Math.round(minCourse));
        }
    }

    private void updateScore() {
        calculateCourse();
        int roundCourse = Math.round(mCourse);
        minCourse =  roundCourse < minCourse ? roundCourse : minCourse;
        long currentTime = System.currentTimeMillis();
        if (currentTime - mPrevUpdateTime > 1000) {
            mPrevUpdateTime = currentTime;
            mScore += 100 / mCourse;
        }
    }

    public CoinObject getCoin() {
        return mCoin;
    }

    public String getCourseString(Context mContext) {
        return mContext.getString(R.string.course, Math.round(mCourse));
    }

    public String getScoreString(Context mContext) {
        return mContext.getString(R.string.score, mScore);
    }

    public void onClick() {
        mCoin.onClick();
    }

    public interface OnGameOverListener {
        void gameOver(int score, int course);
    }
}
