package by.koirn.fallenruble.gameworld.gameobjects;

import by.koirn.fallenruble.AssetsManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

import java.util.Random;

/**
 * Created by dmitry on 24.01.16.
 */
public class CoinObject {

    private final int mWidth;
    private final int mHeight;
    private int mRotation;

    private final Vector2 position;
    private final Vector2 velocity;

    private Random random = new Random();

    public CoinObject(float x, float y, int width, int height) {
        mWidth = width;
        mHeight = height;
        position = new Vector2(x, y);
        velocity = new Vector2(0, 0);
        mRotation = 0;
    }

    public void rotate(float delta) {
        mRotation += 80 * delta;
    }

    public void move(float delta) {
        velocity.add(0, random.nextInt(40));
        position.add(velocity.cpy().scl(delta));
        if (position.y < 1) {
            position.y = 1;
        }
    }

    public synchronized void onClick() {
        velocity.y = -random.nextInt(25);
        position.add(velocity.cpy());
        if (position.y < 1) {
            position.y = 1;
        }
        AssetsManager.sFlip.play();
    }

    public synchronized float getX() {
        return position.x;
    }

    public synchronized float getY() {
        return position.y;
    }

    public float getWidth() {
        return mWidth;
    }

    public float getHeight() {
        return mHeight;
    }

    public int getRotation() {
        return mRotation;
    }

    public void setY(int i) {
        position.y = i;
    }

    public void setX(int i) {
        position.x = i;
    }

    public static int calculateCoinSizeFromScreenSize(int screenWidth, int screenHeight) {
        return Math.min(screenWidth, screenHeight)/ 5;
    }
}
