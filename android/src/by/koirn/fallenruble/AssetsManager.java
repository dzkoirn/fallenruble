package by.koirn.fallenruble;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

/**
 * Created by dmitry on 24.01.16.
 */
public class AssetsManager {

    private static final String BACKGROUND_TEXTURE_FILE = "background_scaled.png";
    private static final String COIN_TEXTURE_FILE = "rubble_asset.png";
    private static final String RULE_TEXTURE_FILE = "rule.png";
    private static final String FONT_NAME = "imperial.ttf";
    private static final String DEAD_SOUND = "dead.wav";
    private static final String FLIP_SOUND = "flap.wav";

    private static final String RUSSIAN_CHARACTERS = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
            + "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";

    private static Texture sBackgroundTexture, sCoinTexture, sRuleTexture;

    public static TextureRegion sBackgroundTextureRegion, sCoinTextureRegion, sRuleTextureRegion;
    public static BitmapFont sFont;
    public static Sound sDead, sFlip;

    public static void load() {
        loadTextures();
        loadFont();
        loadAudio();
    }

    private static void loadTextures() {
        sBackgroundTexture = new Texture(Gdx.files.internal(BACKGROUND_TEXTURE_FILE));
        sBackgroundTexture.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
        sBackgroundTextureRegion = new TextureRegion(sBackgroundTexture);

        sCoinTexture = new Texture(Gdx.files.internal(COIN_TEXTURE_FILE));
        sCoinTexture.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
        sCoinTextureRegion = new TextureRegion(sCoinTexture);
        sCoinTextureRegion.flip(false, true);

        sRuleTexture = new Texture(Gdx.files.internal(RULE_TEXTURE_FILE));
        sRuleTexture.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
        sRuleTextureRegion = new TextureRegion(sRuleTexture);
        sRuleTextureRegion.flip(false, true);
    }

    private static void loadFont() {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal(FONT_NAME));
        FreeTypeFontParameter parameter = new FreeTypeFontParameter();
        parameter.flip = true;
        parameter.size = 36;
        parameter.characters = FreeTypeFontGenerator.DEFAULT_CHARS + RUSSIAN_CHARACTERS;
        sFont = generator.generateFont(parameter);
        generator.dispose();
    }

    private static void loadAudio() {
        sDead = Gdx.audio.newSound(Gdx.files.internal(DEAD_SOUND));
        sFlip = Gdx.audio.newSound(Gdx.files.internal(FLIP_SOUND));
    }

    public static void dispose() {
        // We must dispose of the texture when we are finished.
        sBackgroundTexture.dispose();
        sCoinTexture.dispose();
        sRuleTexture.dispose();
        sFont.dispose();
        sDead.dispose();
        sFlip.dispose();
    }
}
