package by.koirn.fallenruble.screens;

import android.content.Context;
import by.koirn.fallenruble.FallenRubleGame;
import by.koirn.fallenruble.InputHandler;
import by.koirn.fallenruble.gameworld.GameRenderer;
import by.koirn.fallenruble.gameworld.GameWorld;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.ScreenAdapter;

/**
 * Created by dmitry on 24.01.16.
 */
public class GameScreen extends ScreenAdapter implements GameWorld.OnGameOverListener {

    private static final String LOGTAG = GameScreen.class.getSimpleName();

    private GameWorld mWorld;
    private GameRenderer mRenderer;

    public GameScreen(Context mContext) {

        int screenWidth = Gdx.graphics.getWidth();
        int screenHeight = Gdx.graphics.getHeight();

        Gdx.app.log(LOGTAG, screenWidth + " " + screenHeight);

        mWorld = new GameWorld(this, screenWidth, screenHeight);
        mRenderer = new GameRenderer(mContext, screenWidth, screenHeight);
    }

    @Override
    public void render(float delta) {
        mWorld.update(delta);
        mRenderer.renderWorld(mWorld);
    }

    public void onClick() {
        mWorld.onClick();
    }


    @Override
    public void gameOver(int score, int course) {
        if (FallenRubleGame.getGameStateChangesListener() != null) {
            FallenRubleGame.getGameStateChangesListener().onFinish(score, course);
        }
    }
}
