package by.koirn.fallenruble.screens;

import android.content.Context;
import by.koirn.fallenruble.AssetsManager;
import by.koirn.fallenruble.R;
import by.koirn.fallenruble.gameworld.gameobjects.CoinObject;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by dmitry on 31.01.16.
 */
public class GameOverScreen extends ScreenAdapter {

    private final int mScreenWidth, mScreenHeight;
    private final SpriteBatch mBatcher;
    private final CoinObject mCoinObject;
    private final GlyphLayout mFinishGlyphLayout;
    private final float mFinishStringX, mFinishStringY;
    private final GlyphLayout mYourScoreGlyphLayout;
    private final float mYourScoreStringX, mYourScoreStringY;
    private final GlyphLayout mTapToStartGlyphLayout;
    private final float mTapToStartStringX, mTapToStartStringY;

    public GameOverScreen(Context context, int score, int course) {
        mScreenWidth = Gdx.graphics.getWidth();
        mScreenHeight = Gdx.graphics.getHeight();

        int mCoinSize = CoinObject.calculateCoinSizeFromScreenSize(mScreenWidth, mScreenHeight);
        int mCoinX = (mScreenWidth - mCoinSize) / 2;
        int mCoinY = (mScreenHeight - mCoinSize) / 2;
        mCoinObject = new CoinObject(mCoinX, mCoinY, mCoinSize, mCoinSize);

        String finishString = context.getString(R.string.game_finish);
        String yourScore = context.getString(R.string.your_score, score, course);
        String tapTostart = context.getString(R.string.tap_to_start);

        mFinishGlyphLayout = new GlyphLayout(AssetsManager.sFont, finishString);
        mFinishStringX = (mScreenWidth - mFinishGlyphLayout.width) / 2;
        mFinishStringY = mCoinY + mCoinSize + 15;

        mYourScoreGlyphLayout = new GlyphLayout(AssetsManager.sFont, yourScore);
        mYourScoreStringX = (mScreenWidth - mYourScoreGlyphLayout.width) / 2;
        mYourScoreStringY = mFinishStringY + mFinishGlyphLayout.height + 15;

        mTapToStartGlyphLayout = new GlyphLayout(AssetsManager.sFont, tapTostart);
        mTapToStartStringX = (mScreenWidth - mTapToStartGlyphLayout.width) / 2;
        mTapToStartStringY = mYourScoreStringY + mYourScoreGlyphLayout.height + 15;

        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(true, mScreenWidth, mScreenHeight);

        mBatcher = new SpriteBatch();
        // Attach batcher to camera
        mBatcher.setProjectionMatrix(camera.combined);
    }

    @Override
    public void render(float delta) {
        mCoinObject.rotate(delta);

        // We draw a black background. This prevents flickering.
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Begin SpriteBatch
        mBatcher.begin();
        // Disable transparency
        // This is good for performance when drawing images that do not require
        // transparency.
        mBatcher.disableBlending();
        mBatcher.draw(AssetsManager.sBackgroundTextureRegion, 0, 0, mScreenWidth, mScreenHeight);

        mBatcher.enableBlending();

        // Draw coin at its coordinates. Retrieve the Animation object from
        // Pass in the runTime variable to get the current frame.
        mBatcher.draw(AssetsManager.sCoinTextureRegion,
                mCoinObject.getX(), mCoinObject.getY(),
                mCoinObject.getWidth() / 2.0f, mCoinObject.getHeight() / 2.0f,
                mCoinObject.getWidth(), mCoinObject.getHeight(),
                1, 1, mCoinObject.getRotation());

        // Draw text
        AssetsManager.sFont.draw(mBatcher, mFinishGlyphLayout, mFinishStringX, mFinishStringY);
        AssetsManager.sFont.draw(mBatcher, mYourScoreGlyphLayout, mYourScoreStringX, mYourScoreStringY);
        AssetsManager.sFont.draw(mBatcher, mTapToStartGlyphLayout, mTapToStartStringX, mTapToStartStringY);

        // End SpriteBatch
        mBatcher.end();
    }

    @Override
    public void dispose() {
        super.dispose();
        mBatcher.dispose();
        mFinishGlyphLayout.reset();
    }

}
