package by.koirn.fallenruble;

import android.content.Context;
import by.koirn.fallenruble.screens.GameOverScreen;
import by.koirn.fallenruble.screens.GameScreen;
import by.koirn.fallenruble.screens.StartScreen;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;

public class FallenRubleGame extends Game
        implements InputHandler.TouchListener, OnGameStateChangesListener {

	private static final String LOGTAG = FallenRubleGame.class.getSimpleName();

    private static OnGameStateChangesListener mListener;

	final Context mContext;
	final InputHandler mInputHandler;

    private GameState mCurrentState;
    private GameScreen mGameScreen;

	public FallenRubleGame(Context applicationContext) {
		mContext = applicationContext;
		mInputHandler = new InputHandler(this);

        mCurrentState = GameState.READY;
	}

	@Override
	public void create () {
        mListener = this;
		Gdx.app.log(LOGTAG, "create");
		AssetsManager.load();
		setScreen(new StartScreen(mContext));
		Gdx.input.setInputProcessor(new InputHandler(this));
	}

	@Override
	public void dispose() {
		super.dispose();
        mListener = null;
		AssetsManager.dispose();
	}

	@Override
	public void onTouch() {
		switch (mCurrentState) {
            case READY:
                onStart();
                break;
            case RUNNING:
                passClickToScreen();
                break;
            case GAMEOVER:
                onRestart();
                break;
            default:
                break;
        }
	}

    public void passClickToScreen() {
        mGameScreen.onClick();
    }

    @Override
    public void onStart() {
        mCurrentState = GameState.RUNNING;
        mGameScreen = new GameScreen(mContext);
        setScreen(mGameScreen);
    }

    @Override
    public void onRestart() {
        onStart();
    }

    @Override
    public void onFinish(int score, int course) {
        mCurrentState = GameState.GAMEOVER;
        mGameScreen = null;
        setScreen(new GameOverScreen(mContext, score, course));
    }

    public enum GameState {
		READY, RUNNING, GAMEOVER
	}

    public static OnGameStateChangesListener getGameStateChangesListener() {
        return mListener;
    }
}
