package by.koirn.fallenruble;

import by.koirn.fallenruble.gameworld.gameobjects.CoinObject;
import com.badlogic.gdx.InputProcessor;

/**
 * Created by dmitry on 24.01.16.
 */
public class InputHandler implements InputProcessor {

    private TouchListener mListener;

    // Ask for a reference to the Bird when InputHandler is created.
    public InputHandler(final TouchListener listener) {
        mListener = listener;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        mListener.onTouch();
        return true; // Return true to say we handled the touch.
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public interface TouchListener {
        void onTouch();
    }
}
