package by.koirn.fallenruble;

/**
 * Created by dmitry on 31.01.16.
 */
public interface OnGameStateChangesListener {
    void onStart();
    void onRestart();
    void onFinish(int score, int course);
}
